FROM python:3

ADD HelloWorld.py /
ADD Staples_Automation.py /

CMD sudo apt-get install -y unzip xvfb libxi6 libgconf-2-4

# Set up the Chrome PPA
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list

# Update the package list and install chrome
RUN apt-get update -y
RUN apt-get install -y google-chrome-stable

# Set up Chromedriver Environment variables
ENV CHROMEDRIVER_VERSION 2.19
ENV CHROMEDRIVER_DIR /chromedriver
RUN mkdir $CHROMEDRIVER_DIR

# Download and install Chromedriver
RUN wget -q --continue -P $CHROMEDRIVER_DIR "http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip"
RUN unzip $CHROMEDRIVER_DIR/chromedriver* -d $CHROMEDRIVER_DIR

# Put Chromedriver into the PATH
ENV PATH $CHROMEDRIVER_DIR:$PATH

CMD sudo apt-add-repository ppa:qameta/allure
CMD sudo apt-get update 
CMD sudo apt-get install allure
CMD sudo apt-get install python-selenium

RUN pip install -U pytest
RUN pip install -U selenium
RUN pip install -U webdriver_manager
RUN pip install -U allure-pytest
CMD [ "pytest", "-v", "-s", "./HelloWorld.py" ]
CMD pytest -v -s *.py --allure-severities critical --alluredir=allure-results