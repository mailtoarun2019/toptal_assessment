# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: ObjectRepository
# Testcase Name: NA
# Functionality: Object Repository holds the Object information which will be utilized by the Driver script
# Description: The Properties of the Objects are maintained here. User can update the Object properties in one place if any change in the Object
# Author: Arun prakash
# Created on: Apr-15-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


class ObjectRepository:

    # Staples Homepage and Login
    def ico_User_Profile(self):
        return "//*[@id='Sign In']/div/div/div[1]/div"

    def lnk_SignIn(self):
        return "Sign In"

    def txt_UserName(self):
        return "//*[@id='loginUsername']"

    def txt_Password(self):
        return "//*[@id='loginPassword']"

    def btn_SignIn(self):
        return "//*[@id='loginSubmit']"

    def btn_CreateAccount(self):
        return "/html/body/div[1]/div[2]/div/div/form/div/div/div[2]/div/div[7]/div[2]/div"

    def txt_CreateAccountEmail(self):
        return "//input[@id='createEmail']"

    def txt_CreateAccountPassword(self):
        return "//input[@id='createPassword']"

    def btn_CreateAccount_CreateAccountButton(self):
        return "//div[@class='button__button button__fill button__primary button__border_primary']"

    def errmsg_InvalidCreds(self):
        return "customeError"

    def errmsg_BlankUsername(self):
        return "//*[@id='loginUsername.error']"

    def errmsg_BlankPassword(self):
        return "//*[@id='loginPassword.error']"

    def txt_HomepageSearchbar(self):
        return "//*[@id='searchInput']"

    def btn_HomepageSearchButton(self):
        return "button"

    # Add to Cart button
    def btn_product_AddToCart(self):
        return "//*[@id='ctaButton']"

    def label_ProductTitle(self):
        return "//*[@id='product_title']"

    def txt_AddToCart_Quantity(self):
        return "//*[@id='addToCartQty']"

    def btn_ViewCart_fromTempWindow(self):
        return "html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[20]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]"

    def label_CartCount(self):
        return "//*[@id='Cart']/div/a/div[1]/div[2]"

    def txt_ViewCartSearchBar(self):
        return "//input[@id='searchInput']"

    def btn_ViewCartSearchButton(self):
        return "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/button[1]"

    def lnk_ViewCartRemove(self):
        return "button__square_fill"

    def btn_ViewCartCheckout(self):
        return "//div[contains(@class,'cart__checkout_button_section cart__checkout_button_container false cart__right_aligned')]//div[contains(@class,'button__button button__fill button__primary')]"

    def lnk_CheckOutAsGuest(self):
        return "//a[contains(text(),'Checkout as a Guest')]"

    def btn_CheckOutEditButton(self):
        return "/html/body/div[1]/div/div/div/div[2]/div/div[6]/div[1]/div/div[2]/div"

    def txt_CheckOutEmail(self):
        return "//input[@placeholder='Email address*']"

    def txt_CheckOutFirstName(self):
        return "//input[@placeholder='First Name*']"

    def txt_CheckOutLastName(self):
        return "//input[@placeholder='Last Name*']"

    def txt_CheckOutStreetAddr(self):
        return "//input[@placeholder='Street Address*']"

    def txt_CheckOutZip(self):
        return "//input[@placeholder='Zip*']"

    def txt_CheckOutCity(self):
        return "//input[@placeholder='City*']"

    def lst_CheckoutState(self):
        return "//select[@name='address.state']"

    def txt_CheckOutPhone(self):
        return "//input[@placeholder='Phone*']"

    def btn_CheckOutContinue(self):
        return "//div[@class='button__button button__fill button__primary button__border_primary']//span[contains(text(),'Continue')]"

    def btn_AcceptSuggestedAddress(self):
        return "//*[@id='shippingInfoSec']/div/div[3]/div[2]/div/div[2]/div/div[3]/div[2]/div/div/div"

    def btn_PlaceOrder(self):
        return "//*[@id='paymentAndBillingSec']/div/div[2]/div/div[2]/div/div/div[4]/div[3]/div[1]/div/div"

    def lnk_ApplyCoupons(self):
        return "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[4]/div[1]/div/span"

    def txt_CouponCode(self):
        return "//*[@id='coupon_input']"

    def btn_ApplyCoupon(self):
        return "/html/body/div[1]/div/div/div/div[2]/div/div[14]/div[3]/div/div[1]/div[2]/div[2]/div[2]/div[2]/div/div"

    # Security

    def data_Username(self):
        return "ivan.ibhan@aallaa.org"

    def data_Password(self):
        return "Test1234"

    def data_ccNum(self):
        return "4355-3455-2500-2311"

    def data_ccExpiry(self):
        return "02/02"

    def data_ccSecCode(self):
        return "231"

