import pytest
import os
from selenium import  webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from collections import  Callable
import allure
from allure_commons.types import AttachmentType
from ObjRep_Staples import ObjectRepository
from selenium.common.exceptions import NoSuchElementException
from random import randint

class Test_Staples_FunctionalTesting():

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: launchBrowser
# Testcase Name: NA
# Functionality: Launch Browser and Navigate to Website
# Description: Function developed to launch browser. This function will be called before any Pytest Test
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
    @pytest.fixture()
    @allure.step("Launch Browser")
    @allure.description("Launch Browser and Navigate to the URL")
    def launchBrowser(self):
        global driver
        # self.driver= webdriver.Chrome(ChromeDriverManager().install())
        self.driver= webdriver.Chrome()
        self.driver.get("https://www.staples.com/")
        self.driver.maximize_window()
        action = ActionChains(self.driver)

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_LoginAccount_ValidCreds
# Testcase Name: TC_Login_Portal_ValidCredentials
# Functionality: Login
# Description: Validate User is able to Login successfully with Valid Credentials
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------

    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title("Login Staples site with Valid Credentials")
    @allure.step("Launch Browser and attempt Login with Valid Credentials")
    @allure.description("Verify if the user is able to Login successfully with valid credentials")
    def test_LoginAccount_ValidCreds(self,launchBrowser):
        objRepo = ObjectRepository()

        self.click_Link(allure,'UserProfile',objRepo.ico_User_Profile())

        self.click_Link_LinkText(allure,'Sign In',objRepo.lnk_SignIn())

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'button__button button__outline')]")))
        self.set_TextBoxValue(allure,'UserName',objRepo.txt_UserName(),objRepo.data_Username())
        self.set_TextBoxValue(allure,"Password",objRepo.txt_Password(),objRepo.data_Password())
        # self.click_Button(allure,"SignInButton",objRepo.btn_SignIn())
        self.click_Button_Once(allure,"SignInButton",objRepo.btn_SignIn())

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='nucaptcha-media']")))
            if self.driver.find_element_by_xpath("//*[@id='nucaptcha-media']"):
                with allure.step("Login successful but need Captcha Verification"):
                    print ("Login successful but need Captcha Verification")
                assert True, "Login successful but need Captcha Verification"
        except:
            try:
                if self.driver.find_element_by_xpath(objRepo.ico_User_Profile()).is_displayed():
                    with allure.step("Login Successful with Valid Credentials"):
                        print ("Login Successful with Valid Credentials")
                    assert True, "Login Successful with Valid Credentials"
                else:
                    with allure.step("Login UnSuccessful"):
                        print ("Login UnSuccessful")
                    allure.attach(self.driver.get_screenshot_as_png(),name="Login_UnSuccessful",attachment_type=AttachmentType.PNG)
                    self.driver.close()
                    assert False, "Login UnSuccessful"
            except:
                with allure.step("Login UnSuccessful"):
                    print("Login UnSuccessful")
                allure.attach(self.driver.get_screenshot_as_png(), name="Login_UnSuccessful",
                              attachment_type=AttachmentType.PNG)
                self.driver.close()
                assert False, "Login UnSuccessful"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_LoginAccount_InvalidCreds_NoDomain
# Testcase Name: TC_Login_InvalidCredentials_NoDomainEmailID
# Functionality: Login
# Description: Validate the error message displayed when the User tries to Login with Invalid Credentials (Email with no domain)
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Login with Invalid Credentials - No Domain")
    @allure.step("Launch Browser, Validate Error message when attempt to login with Invalid Credentials - No Domain")
    @allure.description("Verify if the user is able to Login with Invalid Credentials")
    def test_LoginAccount_InvalidCreds_NoDomain(self,launchBrowser):
        objRepo = ObjectRepository()

        self.click_Link(allure,'UserProfile',objRepo.ico_User_Profile())

        # WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.LINK_TEXT,"Sign In")))
        # self.driver.find_element_by_link_text("Sign In").click()
        self.click_Link_LinkText(allure,'Sign In',objRepo.lnk_SignIn())

        WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'button__button button__outline')]")))
        self.set_TextBoxValue(allure,'UserName',objRepo.txt_UserName(),"Test")
        self.set_TextBoxValue(allure,"Password",objRepo.txt_Password(),objRepo.data_Password())
        # self.click_Button(allure,"SignInButton",objRepo.btn_SignIn())
        self.click_Button_Once(allure,"SignInButton",objRepo.btn_SignIn())

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.ID,objRepo.errmsg_InvalidCreds())))
            errMsg_InvalidCreds= self.driver.find_element_by_id(objRepo.errmsg_InvalidCreds())
            if errMsg_InvalidCreds.get_attribute('innerHTML')=="We're sorry, but this username and password combination does not match our records. If you do not have a Staples.com account, you will need to create one.":
                with allure.step("Expected error message: Username & Password combination is wrong is displayed"):
                    print ("Expected error message: Username & Password combination is wrong is displayed")
                assert True, "Expected error message: Username & Password combination is wrong is displayed"
            else:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print ("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"
        except:
            try:
                if self.driver.find_element_by_xpath("//*[@id='nucaptcha-media']").is_displayed():
                    with allure.step("Captcha Verification popped up and needed Captcha Verification to proceed"):
                        print ("Captcha Verification popped up and needed Captcha Verification to proceed")
                    assert True, "Captcha Verification popped up and needed Captcha Verification to proceed"
                else:
                    with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                        print("Expected error message: Username & Password combination is wrong is not displayed")
                    assert False, "Expected error message: Username & Password combination is wrong is not displayed"
            except:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_LoginAccount_InvalidCreds_OnlyDomain
# Testcase Name: TC_Login_InvalidCredentials_OnlyDomainEmailID
# Functionality: Login
# Description: Validate the error message displayed when the User tries to Login with Invalid Credentials (Email with only Domain)
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Login with Invalid Credentials - Only Domain")
    @allure.step("Launch Browser, Validate Error message when attempt to login with Invalid Credentials - Only Domain")
    @allure.description("Verify if the user is able to Login with Invalid Credentials")
    def test_LoginAccount_InvalidCreds_OnlyDomain(self,launchBrowser):
        objRepo = ObjectRepository()

        self.click_Link(allure,'UserProfile',objRepo.ico_User_Profile())

        # WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.LINK_TEXT,"Sign In")))
        # self.driver.find_element_by_link_text("Sign In").click()
        self.click_Link_LinkText(allure,'Sign In',objRepo.lnk_SignIn())

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'button__button button__outline')]")))
        self.set_TextBoxValue(allure,'UserName',objRepo.txt_UserName(),"@gmail.com")
        self.set_TextBoxValue(allure,"Password",objRepo.txt_Password(),objRepo.data_Password())
        self.click_Button(allure,"SignInButton",objRepo.btn_SignIn())

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.ID,objRepo.errmsg_InvalidCreds())))
            errMsg_InvalidCreds= self.driver.find_element_by_id(objRepo.errmsg_InvalidCreds())
            if errMsg_InvalidCreds.get_attribute('innerHTML')=="We're sorry, but this username and password combination does not match our records. If you do not have a Staples.com account, you will need to create one.":
                with allure.step("Expected error message: Username & Password combination is wrong is displayed"):
                    print ("Expected error message: Username & Password combination is wrong is displayed")
                assert True, "Expected error message: Username & Password combination is wrong is displayed"
            else:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print ("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"
        except:
            try:
                if self.driver.find_element_by_xpath("//*[@id='nucaptcha-media']").is_displayed():
                    with allure.step("Captcha Verification popped up and needed Captcha Verification to proceed"):
                        print("Captcha Verification popped up and needed Captcha Verification to proceed")
                    assert True, "Captcha Verification popped up and needed Captcha Verification to proceed"
                else:
                    with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                        print("Expected error message: Username & Password combination is wrong is not displayed")
                    assert False, "Expected error message: Username & Password combination is wrong is not displayed"
            except:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"


        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_LoginAccount_InvalidCreds_OnlyNumbers
# Testcase Name: TC_Login_InvalidCredentials_OnlyNumbersEmailID
# Functionality: Login
# Description: Validate the error message displayed when the User tries to Login with Invalid Credentials (Email with only Numbers)
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Login with Invalid Credentials - Only Numbers")
    @allure.step("Launch Browser, Validate Error message when attempt to login with Invalid Credentials - Only Numbers")
    @allure.description("Verify if the user is able to Login with Invalid Credentials")
    def test_LoginAccount_InvalidCreds_OnlyNumbers(self,launchBrowser):
        objRepo = ObjectRepository()

        self.click_Link(allure,'UserProfile',objRepo.ico_User_Profile())

        # WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.LINK_TEXT,"Sign In")))
        # self.driver.find_element_by_link_text("Sign In").click()
        self.click_Link_LinkText(allure,'Sign In',objRepo.lnk_SignIn())

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'button__button button__outline')]")))
        self.set_TextBoxValue(allure,'UserName',objRepo.txt_UserName(),"1234")
        self.set_TextBoxValue(allure,"Password",objRepo.txt_Password(),objRepo.data_Password())
        self.click_Button(allure,"SignInButton",objRepo.btn_SignIn())

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.ID,objRepo.errmsg_InvalidCreds())))
            errMsg_InvalidCreds= self.driver.find_element_by_id(objRepo.errmsg_InvalidCreds())
            if errMsg_InvalidCreds.get_attribute('innerHTML')=="We're sorry, but this username and password combination does not match our records. If you do not have a Staples.com account, you will need to create one.":
                with allure.step("Expected error message: Username & Password combination is wrong is displayed"):
                    print ("Expected error message: Username & Password combination is wrong is displayed")
                assert True, "Expected error message: Username & Password combination is wrong is displayed"
            else:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print ("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"
        except:
            try:
                if self.driver.find_element_by_xpath("//*[@id='nucaptcha-media']").is_displayed():
                    with allure.step("Captcha Verification popped up and needed Captcha Verification to proceed"):
                        print("Captcha Verification popped up and needed Captcha Verification to proceed")
                    assert True, "Captcha Verification popped up and needed Captcha Verification to proceed"
                else:
                    with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                        print("Expected error message: Username & Password combination is wrong is not displayed")
                    assert False, "Expected error message: Username & Password combination is wrong is not displayed"
            except:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_LoginAccount_InvalidCreds_OnlySymbols
# Testcase Name: TC_Login_InvalidCredentials_InvalidSymbolsEmailID
# Functionality: Login
# Description: Validate the error message displayed when the User tries to Login with Invalid Credentials (Email with Invalid Symbols)
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Login with Invalid Credentials - Only Symbols")
    @allure.step("Launch Browser, Validate Error message when attempt to login with Invalid Credentials - Only Symbols")
    @allure.description("Verify if the user is able to Login with Invalid Credentials")
    def test_LoginAccount_InvalidCreds_OnlySymbols(self,launchBrowser):
        objRepo = ObjectRepository()

        self.click_Link(allure,'UserProfile',objRepo.ico_User_Profile())

        # WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.LINK_TEXT,"Sign In")))
        # self.driver.find_element_by_link_text("Sign In").click()
        self.click_Link_LinkText(allure,'Sign In',objRepo.lnk_SignIn())

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'button__button button__outline')]")))
        self.set_TextBoxValue(allure,'UserName',objRepo.txt_UserName(),"!-")
        self.set_TextBoxValue(allure,"Password",objRepo.txt_Password(),objRepo.data_Password())
        self.click_Button(allure,"SignInButton",objRepo.btn_SignIn())

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.ID,objRepo.errmsg_InvalidCreds())))
            errMsg_InvalidCreds= self.driver.find_element_by_id(objRepo.errmsg_InvalidCreds())
            if errMsg_InvalidCreds.get_attribute('innerHTML')=="We're sorry, but this username and password combination does not match our records. If you do not have a Staples.com account, you will need to create one.":
                with allure.step("Expected error message: Username & Password combination is wrong is displayed"):
                    print ("Expected error message: Username & Password combination is wrong is displayed")
                assert True, "Expected error message: Username & Password combination is wrong is displayed"
            else:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print ("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"
        except:
            try:
                if self.driver.find_element_by_xpath("//*[@id='nucaptcha-media']").is_displayed():
                    with allure.step("Captcha Verification popped up and needed Captcha Verification to proceed"):
                        print("Captcha Verification popped up and needed Captcha Verification to proceed")
                    assert True, "Captcha Verification popped up and needed Captcha Verification to proceed"
                else:
                    with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                        print("Expected error message: Username & Password combination is wrong is not displayed")
                    assert False, "Expected error message: Username & Password combination is wrong is not displayed"
            except:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_LoginAccount_InvalidCreds_BlankEmailPassword
# Testcase Name: TC_Login_InvalidCredentials_BlankEmailIDPassword
# Functionality: Login
# Description: Validate the error message displayed when the User tries to Login with Invalid Credentials (Blank Email and Password)
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Login with Invalid Credentials - Blank Email and Password")
    @allure.step("Launch Browser, Validate Error message when attempt to login with Invalid Credentials - Blank Email and Password")
    @allure.description("Verify if the user is able to Login with Invalid Credentials")
    def test_LoginAccount_InvalidCreds_BlankEmailPassword(self,launchBrowser):
        objRepo = ObjectRepository()

        self.click_Link(allure,'UserProfile',objRepo.ico_User_Profile())

        # WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.LINK_TEXT,"Sign In")))
        # self.driver.find_element_by_link_text("Sign In").click()
        self.click_Link_LinkText(allure,'Sign In',objRepo.lnk_SignIn())

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'button__button button__outline')]")))
        self.set_TextBoxValue(allure,'UserName',objRepo.txt_UserName(),"")
        self.set_TextBoxValue(allure,"Password",objRepo.txt_Password(),"")
        self.click_Button(allure,"SignInButton",objRepo.btn_SignIn())

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,objRepo.errmsg_BlankUsername())))
            errMsg_BlankUserName= self.driver.find_element_by_xpath(objRepo.errmsg_BlankUsername()).get_attribute('innerHTML')
            errMsg_BlankPassword= self.driver.find_element_by_xpath(objRepo.errmsg_BlankPassword()).get_attribute('innerHTML')
            if errMsg_BlankUserName == errMsg_BlankPassword=="Required":
                with allure.step("Expected error message: Username & Password Required is displayed"):
                    print ("Expected error message: Username & Password Required is displayed")
                assert True, "Expected error message: Username & Password Required is displayed"
            else:
                with allure.step("Expected error message: Username & Password Required is not displayed"):
                    print ("Expected error message: Username & Password Required is not displayed")
                assert False, "Expected error message: Username & Password Required is not displayed"
        except:
            try:
                if self.driver.find_element_by_xpath("//*[@id='nucaptcha-media']").is_displayed():
                    with allure.step("Captcha Verification popped up and needed Captcha Verification to proceed"):
                        print("Captcha Verification popped up and needed Captcha Verification to proceed")
                    assert True, "Captcha Verification popped up and needed Captcha Verification to proceed"
                else:
                    with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                        print("Expected error message: Username & Password combination is wrong is not displayed")
                    assert False, "Expected error message: Username & Password combination is wrong is not displayed"
            except:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_LoginAccount_InvalidCreds_CorrectEmailWrongPassword
# Testcase Name: TC_Login_InvalidCredentials_InvalidPassword
# Functionality: Login
# Description: Validate the error message displayed when the User tries to Login with Invalid Credentials (Invalid Password)
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Login with Invalid Credentials - Correct Email & Wrong Password")
    @allure.step("Launch Browser, Validate Error message when attempt to login with Invalid Credentials - Correct Email & Wrong Password")
    @allure.description("Verify if the user is able to Login with Invalid Credentials")
    def test_LoginAccount_InvalidCreds_CorrectEmailWrongPassword(self,launchBrowser):
        objRepo = ObjectRepository()

        self.click_Link(allure,'UserProfile',objRepo.ico_User_Profile())

        # WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.LINK_TEXT,"Sign In")))
        # self.driver.find_element_by_link_text("Sign In").click()
        self.click_Link_LinkText(allure,'Sign In',objRepo.lnk_SignIn())

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'button__button button__outline')]")))
        self.set_TextBoxValue(allure,'UserName',objRepo.txt_UserName(),"ivan.ibhan@aallaa.org")
        self.set_TextBoxValue(allure,"Password",objRepo.txt_Password(),"Test")
        self.click_Button(allure,"SignInButton",objRepo.btn_SignIn())

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.ID,objRepo.errmsg_InvalidCreds())))
            errMsg_InvalidCreds= self.driver.find_element_by_id(objRepo.errmsg_InvalidCreds())
            if errMsg_InvalidCreds.get_attribute('innerHTML')=="We're sorry, but this username and password combination does not match our records. If you do not have a Staples.com account, you will need to create one.":
                with allure.step("Expected error message: Username & Password combination is wrong is displayed"):
                    print ("Expected error message: Username & Password combination is wrong is displayed")
                assert True, "Expected error message: Username & Password combination is wrong is displayed"
            else:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print ("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"
        except:
            try:
                if self.driver.find_element_by_xpath("//*[@id='nucaptcha-media']").is_displayed():
                    with allure.step("Captcha Verification popped up and needed Captcha Verification to proceed"):
                        print("Captcha Verification popped up and needed Captcha Verification to proceed")
                    assert True, "Captcha Verification popped up and needed Captcha Verification to proceed"
                else:
                    with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                        print("Expected error message: Username & Password combination is wrong is not displayed")
                    assert False, "Expected error message: Username & Password combination is wrong is not displayed"
            except:
                with allure.step("Expected error message: Username & Password combination is wrong is not displayed"):
                    print("Expected error message: Username & Password combination is wrong is not displayed")
                assert False, "Expected error message: Username & Password combination is wrong is not displayed"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_SearchResults_ValidSearch_Iphone
# Testcase Name: TC_SearchProducts_ValidateSearchResults_Valid_Product1
# Functionality: Searching Products
# Description: Validate that appropriate Search results are retrieved when searching an Item from Home page
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title("Validate Search results are retrieved with Valid Search - Iphone")
    @allure.step("Launch Browser, Search for an item then validate if the appropriate Search results are retrieved")
    @allure.description("Verify Appropriate Search Results are retrieved")
    def test_SearchResults_ValidSearch_Iphone(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),'Iphone')
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))

        item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
        item2 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[2]").get_attribute('innerHTML')
        item3 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[3]").get_attribute('innerHTML')

        if str(item1).find('Apple')>-1 and str(item2).find('Apple')>-1 and str(item3).find('Apple')>-1:
            with allure.step("Search results related to Apple are retrieved successfully"):
                print ("Search results related to Apple are retrieved successfully")
            assert True,"Search results related to Apple are retrieved successfully"
        else:
            with allure.step("Search results related to Apple are not retrieved"):
                print ("Search results related to Apple are not retrieved")
            allure.attach(self.driver.get_screenshot_as_png(),name="Wrong_SearchResults",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Search results related to Apple are not retrieved"


        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_SearchResults_ValidSearch_Pens
# Testcase Name: TC_SearchProducts_ValidateSearchResults_Valid_Product2
# Functionality: Searching Products
# Description: Validate that appropriate Search results are retrieved when searching an Item from Home page
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Search results are retrieved with Valid Search - Pens")
    @allure.step("Launch Browser, Search for an item then validate if the appropriate Search results are retrieved")
    @allure.description("Verify Appropriate Search Results are retrieved")
    def test_SearchResults_ValidSearch_Pens(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),'Pens')
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))

        item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
        item2 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[2]").get_attribute('innerHTML')
        item3 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[3]").get_attribute('innerHTML')

        if str(item1).find('Pen')>-1 and str(item2).find('Pen')>-1 and str(item3).find('Pen')>-1:
            with allure.step("Search results related to Pens are retrieved successfully"):
                print ("Search results related to Pens are retrieved successfully")
            assert True,"Search results related to Pens are retrieved successfully"
        else:
            with allure.step("Search results related to Pens are not retrieved"):
                print ("Search results related to Pens are not retrieved")
            allure.attach(self.driver.get_screenshot_as_png(),name="Wrong_SearchResults_Pens",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Search results related to Pens are not retrieved"


        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_SearchResults_ValidSearch_Notebook
# Testcase Name: TC_SearchProducts_ValidateSearchResults_Valid_Product3
# Functionality: Searching Products
# Description: Validate that appropriate Search results are retrieved when searching an Item from Home page
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Search results are retrieved with Valid Search - Notebook")
    @allure.step("Launch Browser, Search for an item then validate if the appropriate Search results are retrieved")
    @allure.description("Verify Appropriate Search Results are retrieved")
    def test_SearchResults_ValidSearch_Notebook(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),'Notebook')
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))

        item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
        item2 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[2]").get_attribute('innerHTML')
        item3 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[3]").get_attribute('innerHTML')

        if str(item1).find('Notebook')>-1 and str(item2).find('Notebook')>-1 and str(item3).find('Notebook')>-1:
            with allure.step("Search results related to Notebook are retrieved successfully"):
                print ("Search results related to Notebook are retrieved successfully")
            assert True,"Search results related to Notebook are retrieved successfully"
        else:
            with allure.step("Search results related to Notebook are not retrieved"):
                print ("Search results related to Notebook are not retrieved")
            allure.attach(self.driver.get_screenshot_as_png(),name="Wrong_SearchResults_Notebook",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Search results related to Notebook are not retrieved"


        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_SearchResults_InvalidSearch_OnlySymbols
# Testcase Name: TC_SearchProducts_ValidateSearchResults_InvalidSearch
# Functionality: Searching Products
# Description: Validate Error messge displayed when Invalid Search is tried by the User
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Search results for Invalid Search - Only Symbol")
    @allure.step("Launch Browser, Search for an item then validate if the appropriate Search results are retrieved")
    @allure.description("Verify Error message is displayed when Invalid search criteria used for Searching products")
    def test_SearchResults_InvalidSearch_OnlySymbols(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),'-')
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.CLASS_NAME, "NullPage__comNullPageHeading")))
        try:
            if self.driver.find_element_by_class_name("NullPage__comNullPageHeading").is_displayed():
                with allure.step("Error message - Sorry we can't find '-' is displayed as expected"):
                    print ("Error message - Sorry we can't find '-' is displayed as expected")
                assert True, "Error message - Sorry we can't find '-' is displayed as expected"
            else:
                with allure.step("Error message - Sorry we can't find '-' is not displayed as expected"):
                    print ("Error message - Sorry we can't find '-' is not displayed as expected")
                allure.attach(self.driver.get_screenshot_as_png(),name="NoError_Displayed",attachment_type=AttachmentType.PNG)
                self.driver.close()
                assert False, "Error message - Sorry we can't find '-' is not displayed as expected"
        except:
            with allure.step("Error message - Sorry we can't find '-' is not displayed as expected"):
                print("Error message - Sorry we can't find '-' is not displayed as expected")
            allure.attach(self.driver.get_screenshot_as_png(), name="NoError_Displayed",
                          attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Error message - Sorry we can't find '-' is not displayed as expected"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_SearchResults_InvalidSearch_Blank
# Testcase Name: TC_SearchProducts_ValidateSearchResults_BlankSearch
# Functionality: Searching Products
# Description: Validate Application response when User tries to search with Blank value
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Search results for Invalid Search - Blank Value")
    @allure.step("Launch Browser, Search for an item with Blank value and verify no search activity should take place")
    @allure.description("Verify no action on the web page when blank value is used for Searching")
    def test_SearchResults_InvalidSearch_Blank(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            if self.driver.find_element_by_xpath(objRepo.ico_User_Profile()).is_displayed():
                with allure.step('No Search action taken place for Blank value search as expected'):
                    print ('No Search action taken place for Blank value search as expected')
                assert True, "No Search action taken place for Blank value search as expected"
            else:
                with allure.step('Search action taken place for Blank value & Failed'):
                    print ('Search action taken place for Blank value & Failed')
                assert False, "Search action taken place for Blank value & Failed"
        except:
            with allure.step('No Search action taken place for Blank value search as expected'):
                print('No Search action taken place for Blank value search as expected')
            assert True, "No Search action taken place for Blank value search as expected"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_AddToCart_InitialAdd
# Testcase Name: TC_AddToCart_ValidateCartIsIncreased_whenClickingAddtoCartButton
# Functionality: Adding Products to Cart
# Description: Validate that the Cart Icon in the Home page should be Incremented by 1 when Add to Cart button is clicked
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title("Validate Add to Cart functionality - InitialAddToCart")
    @allure.step("Launch Browser, Search Item and Add to Cart to Verify it is incremented")
    @allure.description("Verify Add to Cart functionality is working as expected")
    def test_AddToCart_InitialAdd(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"1")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            cart_Count = self.fetch_substring_from_String(cart_Count)
            if int(cart_Count)==1:
                with allure.step('Only One Item added in the Cart as expected'):
                    print ("Only One Item added in the Cart as expected")
                assert True,"Only One Item added in the Cart as expected"
            else:
                with allure.step('Only One Item was not added in the Cart'):
                    print ("Only One Item was not added in the Cart")
                allure.attach(self.driver.get_screenshot_as_png(),name='Items_inCart_Failed',attachment_type=AttachmentType.PNG)
                self.driver.close()
                assert False, "Only One Item was not added in the Cart"
        except:
            with allure.step('Unable to take the Cart Count'):
                print("Unable to take the Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(), name='Items_inCart_Failed',attachment_type=AttachmentType.PNG)
            assert False, "Unable to take the Cart Count"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_AddToCart_IncrementAdd
# Testcase Name: TC_AddToCart_ValidateCartIsIncreased_fromPreviousValue
# Functionality: Adding Products to Cart
# Description: Validate that the Cart Icon in the Home page should be Incremented by 1 from the Previous Value when Add to Cart button is clicked
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Add to Cart functionality - AddToCartIncrement")
    @allure.step("Launch Browser, Search Item and Add to Cart to Verify it is incremented")
    @allure.description("Verify Add to Cart functionality is working as expected")
    def test_AddToCart_IncrementAdd(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"1")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            initialCart_Count = self.fetch_substring_from_String(cart_Count)
            print (initialCart_Count)
        except:
            with allure.step("Unable to take the Cart Count"):
                print ("Unable to take the Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name='UnableToTakeCartCount',attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Unable to take the Cart Count"

        self.set_TextBoxValue(allure,'ViewCart_SearchBar',objRepo.txt_ViewCartSearchBar(),"leather case")
        self.click_Button(allure,'ViewCart_SearchButton',objRepo.btn_ViewCartSearchButton())

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"1")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH,"/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            finalCart_Count = self.fetch_substring_from_String(cart_Count)
            print (finalCart_Count)

            if int(initialCart_Count)+1==int(finalCart_Count):
                with allure.step('Item in the Cart is incremented as expected. Total items in Cart - '+str(finalCart_Count)):
                    print ("Item in the Cart is incremented as expected. Total items in Cart - "+str(finalCart_Count))
                assert True,"Item in the Cart is incremented as expected. Total items in Cart - "+str(finalCart_Count)
            else:
                with allure.step('Item in the Cart was not incremented as expected. Total items in Cart - '+str(finalCart_Count)):
                    print ("Item in the Cart was not incremented as expected. Total items in Cart - "+str(finalCart_Count))
                allure.attach(self.driver.get_screenshot_as_png(),name='Items_inCartIncr_Failed',attachment_type=AttachmentType.PNG)
                self.driver.close()
                assert False, "Item in the Cart was not incremented as expected. Total items in Cart - "+str(finalCart_Count)
        except:
            with allure.step("Unable to take Cart Count"):
                print ("Unable to take Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name='UnabletoTakeScreenshot',attachment_type=AttachmentType.PNG)
            assert False, "Unable to take Cart Count"
        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_AddToCart_MoreQuantity
# Testcase Name: TC_AddToCart_ValidateMultipleItems_CanbeAdded
# Functionality: Adding Products to Cart
# Description: Validate user is able to Add more than 1 Item to the Cart
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Add to Cart functionality - More Quantity")
    @allure.step("Launch Browser, Search Item and Add to Cart to Verify it is incremented by the Quantity entered")
    @allure.description("Verify Add to Cart functionality is working as expected")
    def test_AddToCart_MoreQuantity(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"3")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            cart_Count = self.fetch_substring_from_String(cart_Count)
            if int(cart_Count)==3:
                with allure.step('Quantity of Item added in the Cart matches - 3'):
                    print ("Quantity of Item added in the Cart matches - 3")
                assert True,"Quantity of Item added in the Cart matches - 3"
            else:
                with allure.step('Quantity of Item added in the Cart does not Match - '+str(cart_Count)):
                    print ("Quantity of Item added in the Cart does not Match - "+str(cart_Count))
                allure.attach(self.driver.get_screenshot_as_png(),name='Items_inCart_Failed',attachment_type=AttachmentType.PNG)
                self.driver.close()
                assert False, "Quantity of Item added in the Cart does not Match - "+str(cart_Count)
        except:
            with allure.step("Unable to take Cart Count"):
                print ("Unable to take Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name="UnableToTakeScreenshot",attachment_type=AttachmentType.PNG)
            assert False, "Unable to take Cart Count"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_AddToCart_NavigateFromViewCart
# Testcase Name: TC_AddToCart_ValidateUserNavigated_ToItemPage_FromViewCart
# Functionality: Adding Products to Cart
# Description: Validate User is able to Navigate to product page by Clicking on the Item in the Cart after Add to Cart
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Add to Cart functionality - Navigate Product from View Cart")
    @allure.step("Launch Browser, Search Item and Add to Cart and Verify if can be Navigated to Product from View Cart")
    @allure.description("Verify Navigating from View Cart functionality is working as expected")
    def test_AddToCart_NavigateFromViewCart(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"1")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            initialCart_Count = self.fetch_substring_from_String(cart_Count)
            print (initialCart_Count)
        except:
            with allure.step("Unable to Take Screenshot"):
                print ("Unable to Take Screenshot")
            allure.attach(self.driver.get_screenshot_as_png(),name="UnableToTakeScreenshot",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Unable to Take Screenshot"

        try:
            self.driver.find_element_by_xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[8]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/p[1]/span[1]").click()
            with allure.step("Navigated to Product page successfully from View Cart"):
                print ("Navigated to Product page successfully from View Cart")
            assert True, "Navigated to Product page successfully from View Cart"
        except:
            with allure.step("Application did not Navigate to Product page from View Cart as expected"):
                print ("Application did not Navigate to Product page from View Cart as expected")
            allure.attach(self.driver.get_screenshot_as_png(),name='Navigate_ViewCart',attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Application did not Navigate to Product page from View Cart as expected"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_AddToCart_ReopenBrowsertoCheckCart
# Testcase Name: TC_AddToCart_ValidateCartItem_RemainsAfterBrowserClose
# Functionality: Adding Products to Cart
# Description: Validate if the Item in Cart gets retained when the Browser is closed and reopened
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Add to Cart functionality - Item present in Cart after Browser Close")
    @allure.step("Launch Browser, Search Item and Add to Cart. Close and Reopen Browser to check if the item is present in the Cart")
    @allure.description("Verify Closing the Browser will clear the Items in the Cart")
    def test_AddToCart_ReopenBrowsertoCheckCart(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"1")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            initialCart_Count = self.fetch_substring_from_String(cart_Count)
            print (initialCart_Count)
        except:
            with allure.step("Unable to Take Cart Count"):
                print ("Unable to Take Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name="UnableToTakeScreenshot",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Unable to Take Cart Count"

        self.driver.close()

        self.driver= webdriver.Chrome(ChromeDriverManager().install())
        self.driver.get("https://www.staples.com/")
        self.driver.maximize_window()
        self.driver.find_element_by_xpath("//div[@class='cart-dropdown__cartIconContainer']").click()

        self.verify_TextInPage(allure,"//span[contains(text(),'Your shopping cart is empty.')]","shopping cart is empty")
        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_AddToCart_OutOfStockMessage
# Testcase Name: TC_AddToCart_ValidateOutOfStockMessageIsDisplayed
# Functionality: Adding Products to Cart
# Description: Validate "Item out of stock" message displayed for Unavailable items and the User should not be able to Purchase the Item
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Add to Cart functionality - Item out of Stock message is displayed for Unavailable Items")
    @allure.step("Launch Browser, Search Out of Stock item and Out of Stock message is displayed")
    @allure.description("Verify Out of Stock message is displayed for Unavailable items")
    def test_AddToCart_OutOfStockMessage(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"touchless automatic liquid soap")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:10]))

        self.verify_TextInPage(allure,"//*[@id='sku_details_container']/div[4]/div[1]/div/span[2]",'item is out of stock for delivery')

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_RemoveCart_RemoveItem
# Testcase Name: TC_RemoveCart_ValidateRemoveItemsCompletely
# Functionality: Removing Products from Cart
# Description: Validate if user is able to Remove item completely when click Remove Item link
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title("Validate Remove Cart functionality - Item should be reduced")
    @allure.step("Launch Browser, Add item and then Click Remove Cart to Reduce Items")
    @allure.description("Verify Items in Cart gets reduced when clicking Remove Cart")
    def test_RemoveCart_RemoveItem(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"1")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            initialCart_Count = self.fetch_substring_from_String(cart_Count)
            print (initialCart_Count)
        except:
            with allure.step("Unable to take Cart Count"):
                print ("Unable to Take Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name="UnableToTakeCartCount",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Unable to take Cart Count"

        self.click_Link(allure,'RemoveCartLink',"//body//div[contains(@class,'')]//div[contains(@class,'')]//div[contains(@class,'')]//div[1]//div[1]//div[2]//div[1]//div[2]//p[1]//a[1]//span[1]")
        self.verify_TextInPage(allure,"//span[contains(text(),'Your shopping cart is empty.')]","shopping cart is empty")
        with allure.step("Added Item was removed after clicking Remove cart link"):
            print ("Added Item was removed after clicking Remove cart link")
        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_RemoveCart_InitialRemove
# Testcase Name: TC_RemoveCart_ValidateSuccessfulRemovalOfItemsFromCart
# Functionality: Removing Products from Cart
# Description: Validate User is able to successfully remove an existing item from Cart
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Remove Cart functionality - One Item should be removed")
    @allure.step("Launch Browser, Add item and then Click Remove Cart to Reduce  Items")
    @allure.description("Verify  Items in Cart gets reduced when clicking Remove Cart")
    def test_RemoveCart_InitialRemove(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"3")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            initialCart_Count = self.fetch_substring_from_String(cart_Count)
            print (initialCart_Count)
        except:
            with allure.step("Unable to take Cart Count"):
                print ("Unable to take Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name="UnableToTakeartcount",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Unable to take Cart Count"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            self.driver.execute_script("document.getElementsByClassName('button__square_fill')[0].click()")
            cart_Count = self.driver.find_element_by_class_name("cart__summary_items_title").get_attribute('innerHTML')
            cart_Count = self.fetch_substring_from_String(cart_Count)
            while int(cart_Count)==int(initialCart_Count):
                cart_Count = self.driver.find_element_by_class_name("cart__summary_items_title").get_attribute('innerHTML')
                cart_Count= self.fetch_substring_from_String(cart_Count)
            # cart_Count = self.driver.find_element_by_class_name("cart__summary_items_title").get_attribute('innerHTML')
            # finalCart_Count = self.fetch_substring_from_String(cart_Count)
            finalCart_Count = cart_Count
            print (finalCart_Count)
        except:
            with allure.step("Unable to Cart Count"):
                print ("Unable to Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name="UnableToTakeCartCount",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Unable to Cart Count"

        if int(initialCart_Count)==int(finalCart_Count)+1:
            with allure.step("Remove Cart button clicked and Item reduced from the Previous count. Prev - "+str(initialCart_Count)+" Curr - "+str(finalCart_Count)):
                print ("Remove Cart button clicked and Item reduced from the Previous count. Prev - "+str(initialCart_Count)+" Curr - "+str(finalCart_Count))
            assert True, "Remove Cart button clicked aint(nd Item reduced from the Previous count. Prev - "+str(initialCart_Count)+" Curr - "+str(finalCart_Count)
        else:
            with allure.step("Remove Cart button was not clicked and Failed"):
                print ("Remove Cart button was not clicked and Failed")
            allure.attach(self.driver.get_screenshot_as_png(),name="Remove_Cart_Fail",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Remove Cart button was not clicked and Failed"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_RemoveCart_MultipleRemove
# Testcase Name: TC_RemoveCart_ValidateMultipleRemovalItems
# Functionality: Removing Products from Cart
# Description: Validate if User is able to remove Multiple items from the Cart
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Remove Cart functionality - Multiple Items should be removed")
    @allure.step("Launch Browser, Add item and then Click Remove Cart to Reduce Multiple Items")
    @allure.description("Verify Multiple Items in Cart gets reduced when clicking Remove Cart")
    def test_RemoveCart_MultipleRemove(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"3")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            initialCart_Count = self.fetch_substring_from_String(cart_Count)
            print (initialCart_Count)
        except:
            with allure.step("Unable to take Cart Count"):
                print ("Unable to take Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name="UnableToTakeartcount",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Unable to take Cart Count"

        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            self.driver.find_element_by_class_name("button__qty_input").send_keys('\b1')

            cart_Count = self.driver.find_element_by_class_name("cart__summary_items_title").get_attribute('innerHTML')
            cart_Count = self.fetch_substring_from_String(cart_Count)
            while int(cart_Count)==int(initialCart_Count):
                cart_Count = self.driver.find_element_by_class_name("cart__summary_items_title").get_attribute('innerHTML')
                cart_Count= self.fetch_substring_from_String(cart_Count)
        except:
            with allure.step("Unable to take Cart Count"):
                print ("Unable to take Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name="UnableToTakeartcount",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Unable to take Cart Count"



        # cart_Count = self.driver.find_element_by_class_name("cart__summary_items_title").get_attribute('innerHTML')
        # finalCart_Count = self.fetch_substring_from_String(cart_Count)
        finalCart_Count = cart_Count
        print (finalCart_Count)

        if int(initialCart_Count)==int(finalCart_Count)+2:
            with allure.step("Remove Cart button clicked and Multiple Items reduced from the Previous count. Prev - "+str(initialCart_Count)+" Curr - "+str(finalCart_Count)):
                print ("Remove Cart button clicked and Multiple Items reduced from the Previous count. Prev - "+str(initialCart_Count)+" Curr - "+str(finalCart_Count))
            assert True, "Remove Cart button clicked and Multiple Items reduced from the Previous count. Prev - "+str(initialCart_Count)+" Curr - "+str(finalCart_Count)
        else:
            with allure.step("Remove Cart button was not clicked and Failed"):
                print ("Remove Cart button was not clicked and Failed")
            allure.attach(self.driver.get_screenshot_as_png(),name="Remove_Cart_Fail",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Remove Cart button was not clicked and Failed"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_CheckoutProcess_ValidPath
# Testcase Name: TC_CheckOutProcess_ValidateUntilOrderPlacement
# Functionality: Checkout Process
# Description: Click on Checkout and proceed with Address Selection & Payment selection until submit
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title("Validate Checkout Process functionality - Happy path validation")
    @allure.step("Add items to cart and proceed with Checkout process")
    @allure.description("Verify if Checkout process is successful")
    def test_CheckoutProcess_ValidPath(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"



        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"3")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
        except:
            print ("Unable to find element for Page identification but Script will proceed with validation")
        # cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
        # initialCart_Count = self.fetch_substring_from_String(cart_Count)
        # print (initialCart_Count)

        self.click_Button(allure,'CheckoutButton',objRepo.btn_ViewCartCheckout())
        self.verify_TextInPage(allure,"/html/body/div[1]/div[2]/div/div/form/div/div/div[1]/div/div/h1","Sign In")
        self.click_Link(allure,'CheckOutAsGuest',"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[7]/div[4]/div[2]/a[1]")
        try:
            while self.driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div/form/div/div/div[1]/div/div/h1").is_displayed():
                self.driver.find_element_by_xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[7]/div[4]/div[2]/a[1]").click()
        except:
            print ("Navigating to Checkout Page")
        # self.click_Link_LinkText(allure,'CheckoutAsGuest',"Checkout as a Guest")

        self.verify_TextInPage(allure,"//div[@class='shipping-tile__addressTitle']","shipping address")

        self.set_TextBoxValue(allure,'Checkout_EmailAddress',objRepo.txt_CheckOutEmail(),'tempmailsetup@tempemail.com')
        self.set_TextBoxValue(allure, 'Checkout_FirstName', objRepo.txt_CheckOutFirstName(),'DummyTest')
        self.set_TextBoxValue(allure, 'Checkout_LastName', objRepo.txt_CheckOutLastName(),'DummyLastName')
        self.set_TextBoxValue(allure, 'CheckOut_StreetAddress', objRepo.txt_CheckOutStreetAddr(),'200 S Randolphville road')
        self.set_TextBoxValue(allure, 'Checkout_ZipCode',objRepo.txt_CheckOutZip(),"08854")
        self.set_TextBoxValue(allure, 'Checkout_City', objRepo.txt_CheckOutCity(), "Piscataway")
        self.set_TextBoxValue(allure, 'Checkout_State',objRepo.lst_CheckoutState(),"New Jersey")
        self.set_TextBoxValue(allure, 'CheckOut_Phone', objRepo.txt_CheckOutPhone(),'774-312-3335')
        self.click_Button(allure,'CheckOut_ContinueButton',objRepo.btn_CheckOutContinue())

        self.click_Button(allure,'UseSuggestedAddressButton',objRepo.btn_AcceptSuggestedAddress())

        self.verify_TextInPage(allure,"/html/body/div[1]/div/div/div/div[2]/div/div[1]/div/div[1]/div/span","Checkout")
        with allure.step("Navigated successfully to Place Order screen. Closing the Webpage now"):
            print ("Navigated successfully to Place Order screen. Closing the Webpage now")
        self.driver.close()
        assert True, "Navigated successfully to Place Order screen. Closing the Webpage now"

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_RemoveCart_UnabletoCheckoutOnEmptyCart
# Testcase Name: TC_CheckOutProcess_ValidateErrorMessageWhenNoItemsInCart
# Functionality: Checkout Process
# Description: When no item in cart, Try to checkout
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate CheckOut Process Functionality - Unable to Checkout with Empty Cart")
    @allure.step("Launch Browser and Ensure the User is not able to Checkout on Empty Cart")
    @allure.description("Verify User is not able to checkout on Empty Cart")
    def test_RemoveCart_UnabletoCheckoutOnEmptyCart(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"



        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"1")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
            cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
            initialCart_Count = self.fetch_substring_from_String(cart_Count)
            print (initialCart_Count)
        except:
            with allure.step("Unable to take Cart Count"):
                print ("Unable to take Cart Count")
            allure.attach(self.driver.get_screenshot_as_png(),name="UnableToTakeartcount",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, "Unable to take Cart Count"

        self.click_Link(allure,'RemoveCartLink',"//body//div[contains(@class,'')]//div[contains(@class,'')]//div[contains(@class,'')]//div[1]//div[1]//div[2]//div[1]//div[2]//p[1]//a[1]//span[1]")
        self.verify_TextInPage(allure,"//span[contains(text(),'Your shopping cart is empty.')]","shopping cart is empty")
        try:
            if self.driver.find_element_by_xpath(objRepo.btn_ViewCartCheckout()).is_displayed():
                with allure.step("Checkout Button is displayed on Empty Cart"):
                    print ("Checkout Button is displayed on Empty Cart")
                allure.attach(self.driver.get_screenshot_as_png(),name='CheckoutButton_EmptyCart_Fail',attachment_type=AttachmentType.PNG)
                self.driver.close()
                assert False, "Checkout Button is displayed on Empty Cart"
            else:
                with allure.step("Checkout Button is not displayed for Empty Cart as Expected"):
                    print ("Checkout Button is not displayed for Empty Cart as Expected")
                assert True, "Checkout Button is not displayed for Empty Cart as Expected"
        except:
            with allure.step("Checkout Button is not displayed for Empty Cart as Expected"):
                print("Checkout Button is not displayed for Empty Cart as Expected")
            assert True, "Checkout Button is not displayed for Empty Cart as Expected"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_CheckoutProcess_NavigateProductPageDuringCheckout
# Testcase Name: TC_CheckOutProcess_ValidateUserTakenTo_ProductPageDuring_Checkout
# Functionality: Checkout Process
# Description: Click on the Item in the Cart during Checkout process
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate if Application Navigates to Product Page during Checkout Process")
    @allure.step("Proceed with Checkout and Verify User is able to Navigate to Product Page during Checkout Process")
    @allure.description("Verify if the User is able to Navigate to Product page during Checkout Process")
    def test_CheckoutProcess_NavigateProductPageDuringCheckout(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"3")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
        except:
            print ("Unable to find element for Page identification but Script will proceed with Validation")
        # cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
        # initialCart_Count = self.fetch_substring_from_String(cart_Count)
        # print (initialCart_Count)

        self.click_Button(allure,'CheckoutButton',objRepo.btn_ViewCartCheckout())
        self.verify_TextInPage(allure,"/html/body/div[1]/div[2]/div/div/form/div/div/div[1]/div/div/h1","Sign In")
        self.click_Link(allure,'CheckOutAsGuest',"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[7]/div[4]/div[2]/a[1]")
        try:
            while self.driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div/form/div/div/div[1]/div/div/h1").is_displayed():
                self.driver.find_element_by_xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[7]/div[4]/div[2]/a[1]").click()
        except:
            print ("Navigating to Checkout Page")

        self.verify_TextInPage(allure,"//div[@class='shipping-tile__addressTitle']","shipping address")

        self.click_Button(allure,'CheckOutEditButton',objRepo.btn_CheckOutEditButton())

        self.verify_TextInPage(allure,"/html/body/div[1]/div/div/div/div[2]/div/div[1]/div/div[1]/div/span","Your Cart")
        with allure.step("Application Navigated back to the Cart from Checkout Process as expected"):
            print ("Application Navigated back to the Cart from Checkout Process as expected")
        assert True, "Application Navigated back to the Cart from Checkout Process as expected"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_CheckoutProcess_OtherItemDisplay
# Testcase Name: TC_CheckOutProcess_ValidateOtherRecommendationsListed_DuringCheckout
# Functionality: Checkout Process
# Description: Before checking verify if other recommendations are also listed for the Customer to Purchase
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate if other items are displayed during Checkout for the User to Purchase")
    @allure.step("Verify if Other Items displayed for User to Purchase")
    @allure.description("Verify if Other items are displayed in Cart / Checkout process for the User to Purchase")
    def test_CheckoutProcess_OtherItemDisplay(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"



        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"3")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
        except:
            print ("Unable to find element for Page identification but script will proceed with validation")

        self.verify_TextInPage(allure,"//*[@id='aac-253']/div/div[1]/span","You may also like")
        self.verify_TextInPage(allure,"/html/body/div[1]/div/div/div/div[2]/div/div[9]/div/div/div/div[2]/div/div/div/div[1]/div/div[1]/div","Customers Also Bought")
        with allure.step("Customers also bought, Personal Recommendations are displayed along with Add to Cart button for the User to Purchase"):
            print ("Customers also bought, Personal Recommendations are displayed along with Add to Cart button for the User to Purchase")
        assert True, "Customers also bought, Personal Recommendations are displayed along with Add to Cart button for the User to Purchase"
        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_CheckoutProcess_ApplyCoupon
# Testcase Name: TC_CheckOutProcess_ValidateDiscountCouponIsApplied
# Functionality: Checkout Process
# Description: During Checkout process check if Discount coupon can be applied
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Apply coupon is displayed and User is able to use it")
    @allure.step("Verify Apply Coupon is available for the User and Utilize it")
    @allure.description("Verify if User is able to use Apply Coupon feature with Valid Coupons")
    def test_CheckoutProcess_ApplyCoupon(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Notebook")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        # self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        # self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"3")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
        except:
            print ("Unable to find element for page identification but script will proceed with validation")

        self.click_Link(allure,'ApplyCouponsLink',objRepo.lnk_ApplyCoupons())
        self.set_TextBoxValue(allure,'CouponTextBox',objRepo.txt_CouponCode(),"81126")
        # self.click_Button(allure,'ApplyCouponButton',objRepo.btn_ApplyCoupon())
        self.click_Button_Once(allure, 'ApplyCouponButton', objRepo.btn_ApplyCoupon())


        self.verify_TextInPage(allure,"/html/body/div[1]/div/div/div/div[2]/div/div[14]/div[3]/div/div[1]/div[2]/div[4]/div/div[1]/div/div/div","Applied coupons")
        with allure.step("Coupons are applied to the Cart Successfully"):
            print ("Coupons are applied to the Cart Successfully")
        assert True, "Coupons are applied to the Cart Successfully"
        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_CheckoutProcess_ValidatePaymentMethods
# Testcase Name: TC_CheckOutProcess_ValidateAllPaymentMethods
# Functionality: Checkout Process
# Description: During Checkout process check if all possible payments, Bank, Credit & Debit cards, Gift cards can be used for payments
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Checkout Process functionality - All Payment Methods are available")
    @allure.step("Validate if all Payment methods are available for User")
    @allure.description("Validate if all Payment methdos are available for the User to Utilize")
    def test_CheckoutProcess_ValidatePaymentMethods(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"3")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
        except:
            print ("Unable to find element for Page identification but script will proceed with validation")
        # cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
        # initialCart_Count = self.fetch_substring_from_String(cart_Count)
        # print (initialCart_Count)

        self.click_Button(allure,'CheckoutButton',objRepo.btn_ViewCartCheckout())
        self.verify_TextInPage(allure,"/html/body/div[1]/div[2]/div/div/form/div/div/div[1]/div/div/h1","Sign In")
        self.click_Link(allure,'CheckOutAsGuest',"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[7]/div[4]/div[2]/a[1]")
        try:
            while self.driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div/form/div/div/div[1]/div/div/h1").is_displayed():
                self.driver.find_element_by_xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[7]/div[4]/div[2]/a[1]").click()
        except:
            print ("Navigating to Checkout Page")
        # self.click_Link_LinkText(allure,'CheckoutAsGuest',"Checkout as a Guest")

        self.verify_TextInPage(allure,"//div[@class='shipping-tile__addressTitle']","shipping address")

        self.set_TextBoxValue(allure,'Checkout_EmailAddress',objRepo.txt_CheckOutEmail(),'tempmailsetup@tempemail.com')
        self.set_TextBoxValue(allure, 'Checkout_FirstName', objRepo.txt_CheckOutFirstName(),'DummyTest')
        self.set_TextBoxValue(allure, 'Checkout_LastName', objRepo.txt_CheckOutLastName(),'DummyLastName')
        self.set_TextBoxValue(allure, 'CheckOut_StreetAddress', objRepo.txt_CheckOutStreetAddr(),'200 S Randolphville road')
        self.set_TextBoxValue(allure, 'Checkout_ZipCode',objRepo.txt_CheckOutZip(),"08854")
        self.set_TextBoxValue(allure, 'Checkout_City', objRepo.txt_CheckOutCity(), "Piscataway")
        self.set_TextBoxValue(allure, 'Checkout_State',objRepo.lst_CheckoutState(),"New Jersey")
        self.set_TextBoxValue(allure, 'CheckOut_Phone', objRepo.txt_CheckOutPhone(),'774-312-3335')
        self.click_Button(allure,'CheckOut_ContinueButton',objRepo.btn_CheckOutContinue())

        self.click_Button(allure,'UseSuggestedAddressButton',objRepo.btn_AcceptSuggestedAddress())

        self.verify_TextInPage(allure,"/html/body/div[1]/div/div/div/div[2]/div/div[1]/div/div[1]/div/span","Checkout")
        self.verify_TextInPage(allure,"//div[contains(text(),'CREDIT CARD')]","CREDIT CARD")
        self.verify_TextInPage(allure,"//div[contains(text(),'OTHER METHODS')]","OTHER METHODS")
        self.verify_TextInPage(allure,"//div[contains(text(),'GIFT CARDS & MORE')]","GIFT CARDS")
        with allure.step("All Payment methods - Credit Cards, Other methods and Gift Cards are available for the User for Payment"):
            print ("All Payment methods - Credit Cards, Other methods and Gift Cards are available for the User for Payment")
        assert True, "All Payment methods - Credit Cards, Other methods and Gift Cards are available for the User for Payment"
        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_CheckoutProcess_ValidateTaxInclusion
# Testcase Name: TC_CheckOutProcess_ValidateTaxesAreIncluded
# Functionality: Checkout Process
# Description: During Checkout process validate Tax is applied for the final amount
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Validate Checkout Process functionality - Tax Applied to the Total")
    @allure.step("Validate if Tax is applied to the Total")
    @allure.description("Validate if the Total Includes all the required Taxes")
    def test_CheckoutProcess_ValidateTaxInclusion(self,launchBrowser):
        objRepo = ObjectRepository()

        self.set_TextBoxValue(allure,'SearchBar',objRepo.txt_HomepageSearchbar(),"Leather case")
        try:
            self.driver.find_element_by_tag_name(objRepo.btn_HomepageSearchButton()).click()
            with allure.step('Search Button clicked successfully'):
                print ("Search button clicked successfully")
            assert True, "Search Button clicked successfully"
        except:
            with allure.step('Search button not clicked'):
                print ("Search button not clicked")
            allure.attach(self.driver.get_screenshot_as_png(),name="SearchButton_notClicked",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,"Search Button not clicked"

        try:
            WebDriverWait(self.driver,5).until(EC.visibility_of_element_located((By.XPATH,"//*[@id='searchTerm']")))
            item1 = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]")
            item1_text = self.driver.find_element_by_xpath("(//a[@class='standard-type__product_title'])[1]").get_attribute('innerHTML')
            print (item1)
            print (item1_text)
            self.click_Link(allure,item1_text,"(//a[@class='standard-type__product_title'])[1]")
            with allure.step('Search results retrieved and Product Opened'):
                print ('Search results retrieved and Product Opened')
            assert True, 'Search results retrieved and Product Opened'
        except:
            with allure.step('Product not Opened as expected'):
                print ('Product not Opened as expected')
            allure.attach(self.driver.get_screenshot_as_png(),name="ProductNot_Opened",attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False, 'Product not Opened as expected'

        self.verify_TextInPage(allure,objRepo.label_ProductTitle(),str(item1_text[:6]))

        self.driver.find_element_by_xpath(objRepo.txt_AddToCart_Quantity()).clear()
        self.set_TextBoxValue(allure,'AddToCart_Quantity',objRepo.txt_AddToCart_Quantity(),"3")
        self.click_Link(allure,'AddToCart',objRepo.btn_product_AddToCart())

        self.click_Button(allure,'ViewCart',objRepo.btn_ViewCart_fromTempWindow())
        try:
            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]")))
        except:
            print ("unable to find page identification but script will proceed with validation")
        # cart_Count = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/div[4]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[1]").get_attribute('innerHTML')
        # initialCart_Count = self.fetch_substring_from_String(cart_Count)
        # print (initialCart_Count)

        self.click_Button(allure,'CheckoutButton',objRepo.btn_ViewCartCheckout())
        self.verify_TextInPage(allure,"/html/body/div[1]/div[2]/div/div/form/div/div/div[1]/div/div/h1","Sign In")
        self.click_Link(allure,'CheckOutAsGuest',"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[7]/div[4]/div[2]/a[1]")
        try:
            while self.driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div/form/div/div/div[1]/div/div/h1").is_displayed():
                self.driver.find_element_by_xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[7]/div[4]/div[2]/a[1]").click()
        except:
            print ("Navigating to Checkout Page")
        # self.click_Link_LinkText(allure,'CheckoutAsGuest',"Checkout as a Guest")

        self.verify_TextInPage(allure,"//div[@class='shipping-tile__addressTitle']","shipping address")

        self.set_TextBoxValue(allure,'Checkout_EmailAddress',objRepo.txt_CheckOutEmail(),'tempmailsetup@tempemail.com')
        self.set_TextBoxValue(allure, 'Checkout_FirstName', objRepo.txt_CheckOutFirstName(),'DummyTest')
        self.set_TextBoxValue(allure, 'Checkout_LastName', objRepo.txt_CheckOutLastName(),'DummyLastName')
        self.set_TextBoxValue(allure, 'CheckOut_StreetAddress', objRepo.txt_CheckOutStreetAddr(),'200 S Randolphville road')
        self.set_TextBoxValue(allure, 'Checkout_ZipCode',objRepo.txt_CheckOutZip(),"08854")
        self.set_TextBoxValue(allure, 'Checkout_City', objRepo.txt_CheckOutCity(), "Piscataway")
        self.set_TextBoxValue(allure, 'Checkout_State',objRepo.lst_CheckoutState(),"New Jersey")
        self.set_TextBoxValue(allure, 'CheckOut_Phone', objRepo.txt_CheckOutPhone(),'774-312-3335')
        self.click_Button(allure,'CheckOut_ContinueButton',objRepo.btn_CheckOutContinue())

        self.click_Button(allure,'UseSuggestedAddressButton',objRepo.btn_AcceptSuggestedAddress())

        self.verify_TextInPage(allure,"/html/body/div[1]/div/div/div/div[2]/div/div[1]/div/div[1]/div/span","Checkout")
        self.verify_TextInPage(allure,"//span[contains(text(),'Estimated tax')]","Estimated tax")
        with allure.step("Estimated Tax for the Purchased Amount is included"):
            print ("Estimated Tax for the Purchased Amount is included")
        assert True, "Estimated Tax for the Purchased Amount is included"
        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_SignUpProcess_ValidCreds
# Testcase Name: TC_UserRegistration_ValidDetails
# Functionality: Sign-In / Registration
# Description: Validate if User is able to Create account with valid details during Signup Process
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title("Verify Signup Process")
    @allure.step("Launch Browser Navigate to URL and verify if Signup possible")
    @allure.description("Validate if user is able to perform Signup Successfully")
    def test_SignUpProcess_ValidCreds(self,launchBrowser):
        objRepo = ObjectRepository()

        self.click_Link(allure,'UserProfile',objRepo.ico_User_Profile())

        self.click_Link_LinkText(allure,'Sign In',objRepo.lnk_SignIn())

        self.click_Button(allure,'CreateAccountButton',objRepo.btn_CreateAccount())

        username = "TestEmailId"+str(randint(10000,99999))+"@aurea.com"
        self.set_TextBoxValue(allure,'CreateAccountUserId',objRepo.txt_CreateAccountEmail(),username)
        self.set_TextBoxValue(allure,'CreateAccountPassword',objRepo.txt_CreateAccountPassword(),"Test1234")
        self.click_Button(allure,'CreateAccountButtonInCreateAccountPage',objRepo.btn_CreateAccount_CreateAccountButton())

        try:
            self.driver.find_element_by_xpath("//*[@id='nucaptcha-media']").is_displayed()
            with allure.step("Account Created Successfully but needs Captcha"):
                print("Account Created Successfully but needs Captcha")
            assert True, "Account Created Successfully but needs Captcha"
        except:
            try:
                self.verify_TextInPage(allure,"//h1[@class='createAccount__customHeader']","Account Created")
                with allure.step("Account Created Successfully"):
                    print ("Account Created Successfully")
                assert True, "Account Created Successfully"
            except:
                with allure.step("Account is not created Successfully"):
                    print("Account is not created Successfully")
                assert True, "Account is not created Successfully"

        self.driver.close()

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: test_SignUpProcess_InvalidPassword
# Testcase Name: TC_UserRegistration_InvalidDetails
# Functionality: Sign-In / Registration
# Description: Validate Error message when user is trying to Create Account with Invalid details
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @allure.severity(allure.severity_level.NORMAL)
    @allure.title("Verify Signup Process - Invalid Password")
    @allure.step("During Signup Process give Invalid New Password to Verify Error message")
    @allure.description("Validate the Error message when Invalid New Password is given during Signup Process")
    def test_SignUpProcess_InvalidPassword(self,launchBrowser):
        objRepo = ObjectRepository()

        self.click_Link(allure,'UserProfile',objRepo.ico_User_Profile())

        self.click_Link_LinkText(allure,'Sign In',objRepo.lnk_SignIn())

        self.click_Button(allure,'CreateAccountButton',objRepo.btn_CreateAccount())

        username = "TestEmailId"+str(randint(10000,99999))+"@aurea.com"
        self.set_TextBoxValue(allure,'CreateAccountUserId',objRepo.txt_CreateAccountEmail(),username)
        self.set_TextBoxValue(allure,'CreateAccountPassword',objRepo.txt_CreateAccountPassword(),"test")
        self.click_Button(allure,'CreateAccountButtonInCreateAccountPage',objRepo.btn_CreateAccount_CreateAccountButton())

        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.ID, "createPassword.error")))
            if self.driver.find_element_by_id("createPassword.error").is_displayed():
                with allure.step("Error message displayed as expected when User enters Invalid Password for Account Signup"):
                    print ("Error message displayed as expected when User enters Invalid Password for Account Signup")
                assert True, "Error message displayed as expected when User enters Invalid Password for Account Signup"
            else:
                with allure.step("Error message displayed is not displayed"):
                    print("Error message displayed is not displayed")
                assert False, "Error message displayed is not displayed"
        except:
            with allure.step("Error message displayed is not displayed"):
                print("Error message displayed is not displayed")
            assert False, "Error message displayed is not displayed"

        self.driver.close()

# ---------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: set_TextBoxValue
# Testcase Name: NA
# Functionality: Set value in Text box
# Description: Checks if the Web Element is present then sets value to the Text box. Handles Reporting as well
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @pytest.mark.skip
    def set_TextBoxValue(self,allure,ObjName,ObjectPath,setValue):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, ObjectPath)))
            self.driver.find_element_by_xpath(ObjectPath).send_keys(setValue)
            with allure.step(ObjName+' is found in the page and Value '+setValue+' is Set'):
                print (ObjName+' is found in the page and Value '+setValue+' is Set')
            assert True, ObjName+" is found in the page and Value "+setValue+" is Set"
        except:
            with allure.step(ObjName+' is not found in the Page'):
                print (ObjName+' is not found in the Page')
            allure.attach(self.driver.get_screenshot_as_png(),name=ObjName+'_notFound',attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,ObjName+" is not found in the page"

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: click_Link
# Testcase Name: NA
# Functionality: Click Link
# Description: Checks if the Web Element is present then Clicks the link based on xpath. Handles Reporting as well
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @pytest.mark.skip
    def click_Link(self,allure,ObjName,ObjectPath):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, ObjectPath)))
            self.driver.find_element_by_xpath(ObjectPath).click()
            with allure.step(ObjName+' is found and clicked'):
                print (ObjName+' is found and clicked')
            assert True, ObjName+" is found and clicked"
        except:
            with allure.step(ObjName+' is not found in the Page'):
                print (ObjName+' is not found in the Page')
            allure.attach(self.driver.get_screenshot_as_png(),name=ObjName+'_notFound',attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,ObjName+" is not found in the page"

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: click_Link_LinkText
# Testcase Name: NA
# Functionality: Click Link
# Description: Checks if the Web Element is present then Clicks the link based on link text. Handles Reporting as well
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @pytest.mark.skip
    def click_Link_LinkText(self,allure,ObjName,ObjectPath):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.LINK_TEXT,ObjectPath)))
            self.driver.find_element_by_link_text(ObjectPath).click()
            with allure.step(ObjName+' is found and clicked'):
                print (ObjName+' is found and clicked')
            assert True, ObjName+" is found and clicked"
            # ctr = 0
            # while self.driver.find_element_by_link_text(ObjectPath).is_displayed() and ctr < 20:
            #     self.driver.find_element_by_link_text(ObjectPath).click()
            #     ctr = ctr + 1


        except:
            with allure.step(ObjName+' is not found in the Page'):
                print (ObjName+' is not found in the Page')
            allure.attach(self.driver.get_screenshot_as_png(),name=ObjName+'_notFound',attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,ObjName+" is not found in the page"

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: click_Button
# Testcase Name: NA
# Functionality: Click Button
# Description: Checks if the Web Element is present then Clicks the Button based on xpath. Handles Reporting as well
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @pytest.mark.skip
    def click_Button(self,allure,ObjName,ObjectPath):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, ObjectPath)))
            self.driver.find_element_by_xpath(ObjectPath).click()
            with allure.step(ObjName+' is found and clicked'):
                print (ObjName+' is found and clicked')
            assert True, ObjName+" is found and clicked"
            try:
                ctr=0
                while self.driver.find_element_by_xpath(ObjectPath).is_displayed() and ctr<20:
                    self.driver.find_element_by_xpath(ObjectPath).click()
                    ctr=ctr+1
            except:
                self.driver.implicitly_wait(3)

        except:
            with allure.step(ObjName+' is not found in the Page'):
                print (ObjName+' is not found in the Page')
            allure.attach(self.driver.get_screenshot_as_png(),name=ObjName+'_notFound',attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,ObjName+" is not found in the page"

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: click_Button_Once
# Testcase Name: NA
# Functionality: Click Button
# Description: Checks if the Web Element is present then Clicks the Button based on xpath. Handles Reporting as well
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @pytest.mark.skip
    def click_Button_Once(self,allure,ObjName,ObjectPath):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, ObjectPath)))
            self.driver.find_element_by_xpath(ObjectPath).click()
            with allure.step(ObjName+' is found and clicked'):
                print (ObjName+' is found and clicked')
            assert True, ObjName+" is found and clicked"

        except:
            with allure.step(ObjName+' is not found in the Page'):
                print (ObjName+' is not found in the Page')
            allure.attach(self.driver.get_screenshot_as_png(),name=ObjName+'_notFound',attachment_type=AttachmentType.PNG)
            self.driver.close()
            assert False,ObjName+" is not found in the page"

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: verify_TextInPage
# Testcase Name: NA
# Functionality: Verify Text in the page
# Description: Checks if the Web Element is present then verifies the mentioned text is present in the web page
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @pytest.mark.skip
    def verify_TextInPage(self,allure,ObjectPath,ObjectValue):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, ObjectPath)))
            itemValue = self.driver.find_element_by_xpath(ObjectPath).get_attribute('innerHTML')
            if str(itemValue).find(ObjectValue)>-1:
                with allure.step(ObjectValue+' found in page'):
                    print (ObjectValue+' found in page')
                assert True, ObjectValue+" found in page"

        except:
            with allure.step(ObjectValue+' not found in the Page'):
                print (ObjectValue+" not found in the Page")
            allure.attach(self.driver.get_screenshot_as_png(),name=ObjName+'_notFoundinPage',attachment_type=AttachmentType.PNG)
            self.driver.close()
            self.driver.quit()
            assert False,ObjectValue+" not found in the Page"

# --------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------
# Function Name: fetch_substring_from_String
# Testcase Name: NA
# Functionality: Fetches substring from a string
# Description: Fetches string from a substring
# Author: Arun prakash
# Created on: Apr-13-2020
# Version: 0.1
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------


    @pytest.mark.skip
    def fetch_substring_from_String(self,textVal):
        start_pos = str(textVal).find("(")
        end_pos = str(textVal).find(")")
        return (textVal[start_pos + 1:end_pos])